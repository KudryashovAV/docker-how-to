# docker-pretty-ps
```sh
git clone https://github.com/politeauthority/docker-pretty-ps.git
cd docker-pretty-ps
python3 setup.py build
python3 setup install
```
Если ошибка
```
ModuleNotFoundError: No module named 'setuptools'
```
то нужно установить python3-setuptools
```
sudo apt-get install python3-setuptools
```