# Работа с Docker API с авторизацией
Для включения Docker REST API необходимо отредактировать docker.service файл добавив аргумент [`-H=tcp://0.0.0.0:2375`](https://stackoverflow.com/questions/42987692/docker-enable-remote-http-api-with-systemd-and-daemon-json) который будут выполнен при старте сервиса. Перезапустим процесс systemd - `systemctl daemon-reload`. Перезапустим сам docker `/etc/init.d/docker restart`
```bash
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock -H=tcp://0.0.0.0:2375
```



Добавим авторизационные данные
```bash
XRA=`echo "{ \"username\": \"kudryashov\", \"password\": \"moiahuitelniiparol\", \"email\": \"kav2k13@gmail.com\" }" | base64 --wrap=0`
```
Скачаем образ
```bash
curl -X POST -d "" -H "X-Registry-Auth: $XRA" http://localhost:2375/images/create?fromImage=harbor.exemple.ru/devops/nginx:1.14.5
```
