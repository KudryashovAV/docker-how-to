#/bin/bash
IPT=`which iptables`
IPS=`which ipset`


$IPS -q --create node_access nethash
$IPS -F node_access
$IPS -A node_access ip/32
$IPS -A node_access ip/32 

$IPT -I DOCKER-USER -i ens6 -m set ! --match-set node_access src -j REJECT
