```sh
docker-compose: error while loading shared libraries: libz.so.1: failed to map segment from shared object: Operation not permitted
```
Solution was to remount tmp with exec permission by executing :
```sh
sudo mount /tmp -o remount,exec
```

https://github.com/docker/compose/issues/1339