#!/bin/bash

MAX_UPTIME=$1

if [ -z ${MAX_UPTIME} ] ; then
  echo Usage: $0 MAX_UPTIME in seconds
  exit 1
fi

function lxc_uptime() {
  local DATE=$1
  local START=$(date '+%s' -d "$( echo $DATE | sed -r 's/(.*)T(..):(..):(..)/\1 \2:\3:\4/')")
  local NOW=$(date '+%s')
  local T=$(($NOW - $START))
  echo $T
}
#
# Stop and remove
#
UP_CONTAINERS=$(docker ps --filter=status=running -q)

for CONTAINER in ${UP_CONTAINERS}; do 
  DATE=$(docker inspect --format='{{.State.StartedAt}}' ${CONTAINER})
  UPTIME=$(lxc_uptime ${DATE})
  NAME=$(docker inspect --format '{{.Name}}' ${CONTAINER})

  CGROUP_PARENT=$(docker inspect --format '{{.HostConfig.CgroupParent}}' ${CONTAINER})

  if [ ${NAME}  == "/x-cadvisor" ]; then
     continue
  fi
  
  if [ ${NAME}  == "/x-nodeexporter" ]; then
     continue
  fi

  if [ ${UPTIME} -gt ${MAX_UPTIME} ]; then
    echo will kill ${CONTAINER}\(${NAME}\) with uptime ${UPTIME}
    docker stop ${CONTAINER}
    docker rm -v ${CONTAINER}
  fi 

done
#
#Cleanup
#
docker system prune -a -f --volumes
