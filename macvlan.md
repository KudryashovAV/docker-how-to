# macvlan
позволяет взять один реальный интерфейс и сделать на его основе несколько виртуальных с разными MAC-адресами и назначить контейнеру ipaddr из сети хоста docker
```bash
SUB_NAME=mynet-shim
ETH_NAME=eth1
SUB_NET=10.99.4.0
SUB_ADDR=10.99.4.223
IP_RANGE=10.99.4.192/27
GATEWAY=10.99.4.1
CONT_IP=10.99.4.201
```
Создаем виртуальный интерфейc, тип macvlan режим bridge
```bash
ip link add ${SUB_NAME} link ${ETH_NAME} type macvlan mode bridge
```
Назначаем ipaddr виртуальному интерфейсу
```bash
ip addr add ${SUB_ADDR}/32 dev ${SUB_NAME}
```
Поднимаем интерфейс
```bash
ip link set ${SUB_NAME} up
```
Создаем маршрут
```bash
ip route add ${IP_RANGE} dev ${SUB_NAME}
```
Create macvlan network
```bash
docker network create -d macvlan -o parent=${ETH_NAME} --subnet ${SUB_NET}/24 --gateway ${GATEWAY} --ip-range ${IP_RANGE} --aux-address host=${SUB_ADDR} mynet
```
Create container
```bash
docker run --net=mynet --ip=${CONT_IP} -itd harbor.ertelecom.ru/web/lawfilter:1.2.0
```
На гипервизоре должен быть отключен `vdsm-no-mac-spoofing`