# Конфигурация-мониторинг памяти и CPU в Docker

Docker Engine в Linux использует технологию, называемую группами управления ([cgroups](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/ch01)).
Поэтому интересующая нас информация находится /sys/fs/cgroup/

Узнаем ID контейнера
```sh
docker inspect 786891b4080a
```
Смотрим записанный максимум использования памяти
```sh
cat /sys/fs/cgroup/memory/docker/786891b4080aed3573b64c8f6314df57ddf05e896c60645edcbed5350e2cf322/memory.max_usage_in_bytes \
| awk '{ foo = $1 / 1024 / 1024 ; print foo "MB" }'
```
Смотрим записанный максимум использования памяти+SWAP
```sh
cat /sys/fs/cgroup/memory/docker/786891b4080aed3573b64c8f6314df57ddf05e896c60645edcbed5350e2cf322/memsw.max_usage_in_bytes \
| awk '{ foo = $1 / 1024 / 1024 ; print foo "MB" }'
```

```sh
docker service update --limit-cpu 0.10 --limit-memory 2g --reserve-cpu 0.05 --reserve-memory 512m nginx-test
```
Что тут написано?-> Для сервиса nginx-test необходимо, чтобы на ноде было доступно 512m ОЗУ, но сервис может использовать не более 2Gb (Аналогично с CPU time резерв 5%, лимит 10%)
**Если при запуски контейнера ресурсов на ноде не достоточно, будет ошибка -> no suitable node (insufficient resources on 1 node)**

- **--cpus 0,5** - 50% от одного любого ядра (**--limit-cpu 0,5**)
- **--cpus 1** - 1 любое ядро
- **--cpus 7,5** - 7 ядер и 50% от 8-го т.д
- **--cpuset-cpus 7** - указывает на работу только на 8 ядре (не работает в swarm)
- **--cpuset-cpus 7 --cpuset-cpus 6** - указывает на работу только на 7 и 8 ядрах (не работает в swarm)

Посмотреть какое едро использует контейнер можно следующим образом:

```
docker top <Container ID>
htop -p <PID>
#В htop, нажмите кнопку F2, перейдите в раздел Columns, найдите пункт PROCESSOR в меню Available Columns и добавьте его (htop ядра считает не с 0, а с 1).
```
- **--memory-swap**:
Если **--memory-swap** установлено то же значение что и **--memory**, контейнер не имеет доступа к swap.
> **Решение работает на одиночном контейнере не реализовано для сервисов в swarm mode (на 9.11.18) Для сервисов можно использовать системные настройки - отключение swap (swapoff -a|swapon -a). Или редактирование swappiness, что не отключает использование swap!

Параметр **vm.swappiness**, который по умолчанию имеет значение 60, и контролирует процент свободной памяти, при которой начнется активный сброс страниц в раздел swap. Иными словами, при 100-60=40% занятой памяти, Ubuntu уже начнет использовать раздел swap. При большом количестве ОЗУ в компьютере, лучше изменить параметр vm.swappiness до меньшего значения. Проверить какое значение можно:
```sh
cat /sys/fs/cgroup/memory/docker/786891b4080aed3573b64c8f6314df57ddf05e896c60645edcbed5350e2cf322/memory.swappiness
или
cat /proc/sys/vm/swappiness
```
Изменить значение **vm.swappiness** для всей системы:
```sh
vim /etc/sysctl.conf
```
Добавить vm.swappiness=10

- Проверка сколько использует контейнер swap
```
docker stats <CONTAINER ID>
docker top <CONTAINER ID>
grep --color wap /proc/<PID>/status
```
- https://habr.com/ru/company/redhatrussia/blog/423051/

- https://docs.docker.com/engine/reference/run/#kernel-memory-constraints
- https://lwn.net/Articles/529927/
- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/ch01
- https://www.datadoghq.com/blog/how-to-collect-docker-metrics/
- https://blog.sleeplessbeastie.eu/2016/12/26/how-to-display-processes-using-swap-space/