# Docker system prune in Docker Swarm

Для очистки можно использовать крон на каждой ноде или использовать сервис с DIND в режиме global с командой очиски
```bash
docker service create --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock --mode=global --restart-condition on-failure docker docker system prune -f
```
Или с рестартом каждые 24 часа
```yaml
  system-prune:
    image: docker
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    command: docker system prune --all --force
    deploy:
      mode: global
      restart_policy:
        delay: 24h
```