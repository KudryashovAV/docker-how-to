# Очистка "мусора" на нодах Docker (при наличии)
Проверяем объем каталога библиотеки динамических данных Docker
```
awk '{print $3}' <<< $(df -h | grep /var/lib/docker)
```
Проверяем активный используемый объем данных (суммируем: запилить скрипт)
```
docker system df
```
Выводим ноду из кластера
```
docker swarm leave -f
```
Останавливаем демон
```
systemctl stop docker
```
На **manager** проверям, что реплики запустились на других нодах
```
docker service ls
```
На **manager** удаляем ноду
```
docker node rm <NODE ID>
```
Удаляем файлы данных Docker
```
rm -rf /var/lib/docker/*
```
Запускам демон
```
systemctl start docker
```
На **manager** получаем токен
```
docker swarm join-token worker
```
Вводим ноду в кластер
```
docker swarm join <token>
```
На **manager** проверяем 
```
docker node ls
```
На **manager** обновляем сервис
```
docker service update <SERVICE NAME> --force
```
Проверяем, контейнер развернут на worker-е 
```
docker ps 
```
!Ахтунг, по дефолту ограничений по логу нет и он может сожрать все место.
Чистим лог если он неибически вырос
```sh
echo "" > $(docker inspect --format='{{.LogPath}}' <container_name_or_id>)
```
[и не забиваем, а настраевам ротацию логов](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/logrotate.md)