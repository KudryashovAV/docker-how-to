# List all repo
```bash
curl -s GET http://127.0.0.1:5000/v2/_catalog?n=2000 | sed 's/.*.:.//g'| sed 's/\]}//g' | sed 's/,/\n/g' | sort
```
# List tags
```bash
curl -s "127.0.0.1:5000/v2/common/php-5.4-fpm/tags/list" | sed 's/.*tags.:.//g' | sed 's/\]}//g' | sed 's/"//g' | sed 's/,/\n/g'| sort | uniq
```

# [Чистим реджистри](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/cleanup.sh)  
После удаления тегов место в файловой системе не освобождается, для освобождения места необходимо запустить garbage collector:
```bash
docker exec -it dhub_registry_1 bin/registry garbage-collect /etc/docker/registry/config.yml
```
---
https://docs.docker.com/registry/spec/api/#detail  
[swagger](https://gitlab.com/KudryashovAV/docker-how-to/blob/master/docker-registry.yaml)